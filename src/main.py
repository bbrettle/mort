
import arcade
from character_sprite import CharacterBase, CharacterHead, CharacterBody
from constants import *


class MyGame(arcade.Window):
    def __init__(self, width, height, title):
        super().__init__(width, height, title)

        # Variables that will hold sprite lists
        self.player_frame_list = None

        # sprites
        self.player_base = None
        self.player_head = None
        self.player_body = None

        # Track the current state of what key is pressed
        self.left_pressed = False
        self.right_pressed = False
        self.up_pressed = False
        self.down_pressed = False

        # Set the background color
        arcade.set_background_color(arcade.color.AMAZON)

    def setup(self):
        # Sprite lists
        self.player_frame_list = arcade.SpriteList()
        self.player_head_list = arcade.SpriteList()
        self.player_body_list = arcade.SpriteList()

        # create sprites
        self.player_base = CharacterBase("assets\\blank.png", SPRITE_SCALING)
        self.player_head = CharacterHead()
        self.player_body = CharacterBody()

        # starting positions
        self.player_base.center_x = 50
        self.player_base.center_y = 50
        self.player_head.center_x = self.player_base.center_x
        self.player_head.center_y = self.player_base.center_y
        self.player_body.center_x = self.player_base.center_x
        self.player_body.center_y = self.player_base.center_y


        # adding sprites to lists
        self.player_frame_list.append(self.player_base)
        self.player_head_list.append(self.player_head)
        self.player_body_list.append(self.player_body)

    def on_draw(self):
        arcade.start_render()

        # draw sprites.
        self.player_frame_list.draw()
        self.player_base.draw_hit_box(arcade.color.RED)
        self.player_head_list.draw()
        self.player_body_list.draw()

    def on_update(self, delta_time):
        self.player_body.center_x = self.player_base.center_x
        self.player_body.center_y = self.player_base.center_y
        self.player_head.center_x = self.player_base.center_x
        self.player_head.center_y = self.player_base.center_y + 4

        self.player_base.change_x = 0
        self.player_base.change_y = 0
        self.player_head.change_x = 0
        self.player_head.change_y = 0
        self.player_body.change_x = 0
        self.player_body.change_y = 0

        # player move logic
        if self.up_pressed and not self.down_pressed:
            self.player_base.change_y = MOVEMENT_SPEED
            self.player_head.change_y = MOVEMENT_SPEED
            self.player_body.change_y = MOVEMENT_SPEED
        elif self.down_pressed and not self.up_pressed:
            self.player_base.change_y = -MOVEMENT_SPEED
            self.player_head.change_y = -MOVEMENT_SPEED
            self.player_body.change_y = -MOVEMENT_SPEED
        if self.left_pressed and not self.right_pressed:
            self.player_base.change_x = -MOVEMENT_SPEED
            self.player_head.change_x = -MOVEMENT_SPEED
            self.player_body.change_x = -MOVEMENT_SPEED
        elif self.right_pressed and not self.left_pressed:
            self.player_base.change_x = MOVEMENT_SPEED
            self.player_head.change_x = MOVEMENT_SPEED
            self.player_body.change_x = MOVEMENT_SPEED

        # call list updates
        self.player_frame_list.update()
        self.player_head_list.update()
        self.player_body_list.update()

    def on_key_press(self, key, modifiers):

        if key in KEY_UP:
            self.up_pressed = True
        elif key in KEY_DOWN:
            self.down_pressed = True
        elif key in KEY_LEFT:
            self.left_pressed = True
        elif key in KEY_RIGHT:
            self.right_pressed = True

    def on_key_release(self, key, modifiers):

        if key in KEY_UP:
            self.up_pressed = False
        elif key in KEY_DOWN:
            self.down_pressed = False
        elif key in KEY_LEFT:
            self.left_pressed = False
        elif key in KEY_RIGHT:
            self.right_pressed = False

    def on_mouse_press(self, x, y, button, key_modifiers):
        if button:
            print("left click pressed")

    def on_mouse_release(self, x: float, y: float, button: int, modifiers: int):
        if button:
            print("left click released")

def main():
    """ Main method """
    window = MyGame(SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_TITLE)
    window.setup()
    arcade.run()


if __name__ == "__main__":
    main()
