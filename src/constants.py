import arcade

# screen settings
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600
SCREEN_TITLE = "Mort"

# player scale
SPRITE_SCALING = 2

# animation
UPDATES_PER_FRAME = 10
SLOW_UPDATES_PER_FRAME = 18
WALK_FRAME_COUNT = 8

# Constants used to track if the player is facing left or right
RIGHT_FACING = 1
LEFT_FACING = 0

# default player move speed
MOVEMENT_SPEED = 5

# Key mappings
KEY_UP = [arcade.key.UP, arcade.key.W]
KEY_DOWN = [arcade.key.DOWN, arcade.key.S]
KEY_LEFT = [arcade.key.LEFT, arcade.key.A]
KEY_RIGHT = [arcade.key.RIGHT, arcade.key.D]
INVENTORY = [arcade.key.I]
SEARCH = [arcade.key.E]

