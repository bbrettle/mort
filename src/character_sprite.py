
import arcade
from constants import *


def load_texture_pair(filename):
    """
    Load a texture pair, with the second being a mirror image.
    """
    return [
        arcade.load_texture(filename),
        arcade.load_texture(filename, flipped_horizontally=True)
    ]


class CharacterBase(arcade.Sprite):
    def __init__(self, filename, scale):
        super().__init__(filename, scale)
        self.character_face_direction = LEFT_FACING
        self.points = [[-8, -16], [-8, 16], [8, 16], [8, -16]]

    def update(self):
        self.center_x += self.change_x
        self.center_y += self.change_y


class CharacterBody(arcade.Sprite):
    def __init__(self):
        super().__init__()

        self.character_face_direction = RIGHT_FACING
        self.cur_texture = 0
        self.scale = SPRITE_SCALING

        self.idle_textures = []
        self.walk_textures = []

        for i in range(1, 5):
            idle_texture = load_texture_pair(f"assets\\body\\idle\\body_idle{i}.png")
            self.idle_textures.append(idle_texture)

        for i in range(1, 9):
            texture = load_texture_pair(f"assets\\body\\walk\\body_walk{i}.png")
            self.walk_textures.append(texture)

    def update(self):

        # Figure out if we need to flip face left or right
        if self.change_x < 0 and self.character_face_direction == RIGHT_FACING:
            self.character_face_direction = LEFT_FACING
        elif self.change_x > 0 and self.character_face_direction == LEFT_FACING:
            self.character_face_direction = RIGHT_FACING


        self.cur_texture += 1

        # Idle animation
        if self.change_x == 0 and self.change_y == 0:
            if self.cur_texture > 3 * UPDATES_PER_FRAME:
                self.cur_texture = 0
            frame = self.cur_texture // UPDATES_PER_FRAME
            direction = self.character_face_direction
            self.texture = self.idle_textures[frame][direction]
            return

        # Walking animation
        if self.cur_texture > 7 * UPDATES_PER_FRAME:
            self.cur_texture = 0
        frame = self.cur_texture // UPDATES_PER_FRAME
        direction = self.character_face_direction
        self.texture = self.walk_textures[frame][direction]


class CharacterHead(arcade.Sprite):
    def __init__(self):
        super().__init__()

        self.character_face_direction = RIGHT_FACING
        self.cur_texture = 0
        self.scale = SPRITE_SCALING

        self.idle_textures = []
        self.walk_textures = []

        for i in range(1, 4):
            idle_texture = load_texture_pair(f"assets\\head\\head_{i}.png")
            self.idle_textures.append(idle_texture)

        texture = load_texture_pair(f"assets\\head\\head_1.png")
        self.walk_textures.append(texture)

    def update(self):

        # Figure out if we need to flip face left or right
        if self.change_x < 0 and self.character_face_direction == RIGHT_FACING:
            self.character_face_direction = LEFT_FACING
        elif self.change_x > 0 and self.character_face_direction == LEFT_FACING:
            self.character_face_direction = RIGHT_FACING

        self.cur_texture += 1

        # Idle animation
        if self.change_x == 0 and self.change_y == 0:
            if self.cur_texture > 2 * SLOW_UPDATES_PER_FRAME:
                self.cur_texture = 0
            frame = self.cur_texture // SLOW_UPDATES_PER_FRAME
            direction = self.character_face_direction
            self.texture = self.idle_textures[frame][direction]
            return

        if self.cur_texture > 0 * UPDATES_PER_FRAME:
            self.cur_texture = 0
        frame = self.cur_texture // UPDATES_PER_FRAME
        direction = self.character_face_direction
        self.texture = self.walk_textures[frame][direction]